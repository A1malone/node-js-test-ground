const express = require("express");
const app = express();
const path = require("path");
const dotenv = require("dotenv");
const connectDB = require("./public/db");

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

//Load env
dotenv.config({
  path: "./config.env",
});

//Connect to MongoDB
connectDB();

//api routes -- use above the below route
app.use("/landingCF", require("./routes/email"));
app.use("/Project-Request", require("./routes/projectRequest"));

//send xml file
app.use(express.static(path.join(__dirname, "build")));
app.get("/sitemap.xml", function (req, res) {
  res.sendFile(path.join(__dirname, "build", "sitemap.xml"));
});

/* line for react always keep at the bottem of routes
app.get("*", function (req, res) {
  res.sendFile(path.join(__dirname, "build", "index.html"));
});
*/
app
  .listen(port, (err) => {
    if (err) throw err;
    console.log(
      `> Server running in ${
        dev === true ? "dev" : "production"
      } mode and on port ${port}`
    );
  })
  .catch((ex) => {
    console.error(ex.stack);
    process.exit(1);
  });
