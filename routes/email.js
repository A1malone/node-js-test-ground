const express = require("express");
const router = express.Router();
const nodemailer = require("nodemailer");
const dotenv = require("dotenv");
var cors = require("cors");

dotenv.config({
  path: "../config.env",
});

router.use(cors());

router.post("/", (req, res) => {
  //connect to host
  let transport = nodemailer.createTransport({
    host: process.env.Email_HOST,
    port: process.env.Email_PORT,
    auth: {
      user: process.env.Email_USER,
      pass: process.env.Email_PASSWORD,
    },
  });

  const message = {
    from: req.body.email, // Sender address
    to: "alfred@alfredmalone.com", // List of recipients
    subject: `Message from ${req.body.name} at portfolio site`, // Subject line
    text: req.body.message, // Plain text body
  };

  transport.sendMail(message, (err, info) => {
    if (err) {
      console.log(err);
    } else {
      res.redirect("/");
      console.log(info);
    }
  });
});

module.exports = router;
