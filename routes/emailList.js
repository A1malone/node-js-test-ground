const express = require("express");
const router = express.Router();
const Email = require("../models/email");
const dotenv = require("dotenv");

dotenv.config({
  path: "../config.env"
});

//@route    Post email
//@desc     Lists of emails for notify me
//@access   Public

router.post("/", async (req, res) => {
  try {
    // looks existing emails
    const emailCheck = await Email.findOne({ email: req.body.email });

    if (emailCheck) {
      return res.send("Email already exists");
    }

    let addEmail = new Email({ email: req.body.email });

    //saves user in database
    await addEmail.save().then(res.redirect(req.body.redirectURL));
  } catch (err) {
    console.error(`Notify-Me Error: ${err.message}`);
    res.status(500).send(`Notify-Me Error: ${err.message}`);
  }
});

module.exports = router;
