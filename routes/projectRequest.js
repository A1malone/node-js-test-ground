const express = require("express");
const router = express.Router();
const nodemailer = require("nodemailer");
const dotenv = require("dotenv");
var cors = require("cors");

dotenv.config({
  path: "../config.env",
});

router.use(cors());

router.post("/", (req, res) => {
  //connect to host
  let transport = nodemailer.createTransport({
    host: process.env.Email_HOST,
    port: process.env.Email_PORT,
    auth: {
      user: process.env.Email_USER,
      pass: process.env.Email_PASSWORD,
    },
  });

  const name = req.body.name;
  const email = req.body.email;
  const website = req.body.website;
  const referral = req.body.referral;
  const budget = req.body.budget;
  const completion = req.body.completion;
  const prjtDesc = req.body.ProjectDescription;
  const content = `Name: ${name} \n Email: ${email}
  \n Website: ${website} \n Referral: ${referral}
  \n Budget: ${budget} \n Completion: ${completion}
  \n Description: ${prjtDesc}
  `;

  const message = {
    from: email, // Sender address
    to: "alfred@alfredmalone.com", // List of recipients
    subject: `Request from ${name} at portfolio site`, // Subject line
    text: content, // Plain text body
  };

  transport.sendMail(message, (err, info) => {
    if (err) {
      console.log(err);
    } else {
      res.redirect("/");
      console.log(info);
    }
  });
});

module.exports = router;
