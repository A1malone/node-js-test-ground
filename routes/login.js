const express = require("express");
const router = express.Router();
const User = require("../models/user");
const bcrypt = require("bcryptjs");
const dotenv = require("dotenv");
const path = require("path");

dotenv.config({
  path: "../config.env"
});

router.get("/", (req, res) =>
  res.sendFile(path.join(__dirname + "/views/login.html"))
);

//@route    Post api/user
//@desc     Register user
//@access   Public

router.post("/register", async (req, res) => {
  try {
    //encrypt password
    const salt = await bcrypt.genSalt(10);

    const hashed = await bcrypt.hash(req.body.password, salt);

    const user = {
      name: req.body.first_name + " " + req.body.last_name,
      password: hashed,
      email: req.body.email,
      date: Date.now()
    };
    let newUser = new User(user);

    //saves user in database
    await newUser.save().then(res.redirect("/login"));
  } catch (err) {
    console.error(`Registration Error: ${err.message}`);
    res.status(500).send(`Registration Error: ${err.message}`);
  }
});

module.exports = router;

//@route    POST user
//@desc     Login
//@access   Public

router.post("/SignIn", async (req, res) => {
  try {
    const user = await User.findOne({ email: req.body.username });

    if (user.email == null) {
      return res.status(400).send("cant find user");
    }
    if (await bcrypt.compare(req.body.login_password, user.password)) {
      res.send("success for now");
    } else {
      res.send("invalid password");
    }
  } catch (err) {
    console.error(err.message);
    res.status(500).send(`Login Error: ${err.message}`);
  }
});

module.exports = router;
