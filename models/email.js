const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const emailSchema = new Schema({
  email: {
    type: String,
    required: true,
    unique: true
  }
});

module.exports = User = mongoose.model("Email", emailSchema);
