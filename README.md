# Node JS Test Ground

Test area for current and new features of my Node Js server.

## Installation

Clone this repository to install.

## Current Features

- email
- database connection / modeling

## Future Features

- User authentication
- Dashboard
- dynamic routing
- pagination
- blog

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)
