const mongoose = require("mongoose");
const dotenv = require("dotenv");

dotenv.config({
  path: "../config.env"
});

const connectDB = async () => {
  try {
    await mongoose.connect(process.env.mongoURI, {
      useNewUrlParser: true,
      useCreateIndex: true,
      useUnifiedTopology: true
    });

    console.log("mongoDb Connected");
  } catch (err) {
    console.log(`DB Connection Error: ${err.message}`);
    //Exit process with failure
    process.exit(1);
  }
};

module.exports = connectDB;
